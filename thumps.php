#!/usr/bin/php
<?php
proc_nice(19);
ini_set('memory_limit','-1');
declare(ticks = 100);
pcntl_signal(SIGTERM, "sig_handler");
pcntl_signal(SIGHUP,  "sig_handler");
pcntl_signal(SIGINT, "sig_handler");

echo "Starttin sheduler script.";
function sig_handler($signo) {

     switch ($signo) {
         case SIGTERM:
             // handle shutdown tasks
             exit;
             break;
         case SIGHUP:
             // handle restart tasks
             break;
         case SIGUSR1:
             echo "Caught SIGUSR1...\n";
             break;
         default:
             exit;
	     break;
     }

}

function file_get_contents_utf8($fn) {
     $content = file_get_contents($fn);
      return mb_convert_encoding($content, 'ISO-8859-1','UTF-8');
}

error_reporting(E_ERROR);


if (isset($argv[1])) {
	$arc=$argv[1];
	echo "ARC $arc\n";
}else{
   echo "ARC numero tai all puuttuu!\n";
   exit;
   break;
}

if ($argv[2]=="retry") {
	echo "Retry\n";
	$not_in="select sivu_id from apu.thumb where status in ('OK','ohita')";
} else {
	$not_in="select sivu_id from apu.thumb where status in ('OK','ohita','Kasittelyssa','Ei saatu ehjaa access kuvaa')";
}

$conn = oci_connect('diona', 'diona', 'pythia/dionapdb.docworks.lib.helsinki.fi');

$select="select s.sivu_id,s.nide_id,s.juoksevanumero,t.uri,t.tiedosto_id,f.tiedostomuoto
			from nide n, sivu s,tiedosto t, formaatti f
			where n.nide_id=s.nide_id 
			and f.formaatti_id=t.formaatti_id 
			and t.tiedosto_id=s.access_image_id 
			and t.formaatti_id in ('1','2','5','6','7','8','11')
			and n.mets_tiedosto_id is not null
			and t.uri not like '%fd2020-%'
			and t.uri not like '%fd2019-%'";
if ($arc!="all") {
	$select=$select." and uri like '/".$arc."/%' ";
} 
$select=$select."
and s.sivu_id not in ( $not_in ) 
order by s.juoksevanumero asc, nide_id desc";

echo "$select\n";

$stid = oci_parse($conn, $select);
$res1=oci_execute($stid);

if (!$res1) {
	echo "error:".oci_error()."--";
	exit(9);
}

while (($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) != false) {
	$starttime = microtime(true);
	$tied_id=$row['TIEDOSTO_ID'];
	$uri=$row['URI'];
	$sivu_id=$row['SIVU_ID'];
	$nide_id=$row['NIDE_ID'];
	$juoksevanumero=$row['JUOKSEVANUMERO'];
	$tiedostomuoto=strtolower($row['TIEDOSTOMUOTO']);
	$prefix="http://localhost/cgi-bin/zipcgi/zip.cgi?path=images/";
	$filename=$prefix.$uri;
	$lvl1=substr($nide_id,0,2);
	$lvl2=substr($nide_id,2,2);
	$base="/mnt/export/thumb";	
	$tempname=$base."/tmp/".$nide_id."-".$juoksevanumero.".".$tiedostomuoto;
	$thumbpath=$base."/".$lvl1."/".$lvl2;	
	$thumbname2=$base."/".$lvl1."/".$lvl2."/".$nide_id."-".$juoksevanumero.".jpg";	
	echo "$sivu_id :";
	
	$delete ="delete from apu.thumb where SIVU_ID='$sivu_id'";
	
	$stmt2 = oci_parse($conn, $delete);
	$res=oci_execute($stmt2, OCI_COMMIT_ON_SUCCESS);
	if (!$res) {
		echo "error:".oci_error();
		exit(9);
	}
	oci_free_statement($stmt2);
	
	$insert ="insert into apu.thumb values ('$nide_id','$sivu_id','$juoksevanumero','$filename',null,null,null, 'Kasittelyssa', SYSDATE,null)";
	
	$stmt3 = oci_parse($conn, $insert);
	$res=oci_execute($stmt3, OCI_COMMIT_ON_SUCCESS);
	if (!$res) {
		echo "error:".oci_error();
		exit(9);
	}
	oci_free_statement($stmt3);
	
	mkdir($thumbpath,0777,true);
	$content = file_get_contents($filename);
	file_put_contents($tempname,$content);
	
	if ($tiedostomuoto=="jp2") {
		$tempname2=str_replace(".jp2",".bmp",$tempname);
		exec ("jasper --input $tempname --output $tempname2");
		unlink ($tempname);
		$tempname=$tempname2;
	}
	
	$starttime_style = microtime(true);
	
	try {
		$image= new Imagick($tempname);
	}catch (ImagickException $e) {
	
		echo "Rikki : $nide_id - $sivu_id - $uri - $e";
		$update ="update apu.thumb set STATUS='Ei saatu ehjaa access kuvaa', SOURCEURI='$filename', AIKA=SYSDATE where SIVU_ID='$sivu_id'";
		echo "$update\n";
		
		$stmt4 = oci_parse($conn, $update);
		$res=oci_execute($stmt4, OCI_COMMIT_ON_SUCCESS);
		if (!$res) {
			echo "error:".oci_error();
			exit(9);
		}
		oci_free_statement($stmt4);
		continue;

	}
	
	$image->thumbnailImage(300,300,true);
	$image->unsharpMaskImage(50 , 0.2 , 20 , 0.0);
	$image->unsharpMaskImage(1 , 0.2 , 0.8 , 0.0);
	$image->setImageFormat("jpg"); 
	$image->setCompression(Imagick::COMPRESSION_JPEG);
	$image->setCompressionQuality(100);
	$image->writeImage($thumbname2);
	$image->removeImage();
	unlink($tempname);
	
	$update2 ="update apu.thumb set STATUS='OK', DESTURI='$thumbname2', AIKA=SYSDATE, KUVAUS=null where SIVU_ID='$sivu_id'";
	$stmt5 = oci_parse($conn, $update2);
	$res=oci_execute($stmt5, OCI_COMMIT_ON_SUCCESS);
	if (!$res) {
		echo "error:".oci_error();
		exit(9);
	}
	oci_free_statement($stmt5);
	
	
	$endtime = microtime(true);
	$timediff = round($endtime - $starttime,1);
	echo " $timediff - ".$nide_id."-",$juoksevanumero," - ",$filename,"\n";

}
oci_free_statement($stid);
oci_close($conn);
exit(0);
?>
