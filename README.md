# README #

Scripti tekee kerran päivässä ajastetusti valmiiksi skannattuiden töiden sivuista thubnails kuvat.

Scriptiä voi ajaa komentoriviltä esim -> "php thumps.php all"

Ajastus löytyy selene palvelimelta. Mahdolliset pääsyt, tunnarit ym.. saa Riku Paanaselta.
itse ajastus löytyy käskyllä "crontab -l"

# Uudet thumbnailit
45 1 * * * /home/thor/tietokantaskriptit/thumbs.php all > /tmp/thumbs.log 2>&1
MAILTO=""
